/*
  Copyright (c) 2018-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.contact;

import android.net.Uri;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

public class ContactViewModel extends ViewModel {
  private static final String STATE_CONTACT = "contact";
  private final SavedStateHandle state;
  private Uri contact;

  public ContactViewModel(SavedStateHandle state) {
    this.state = state;
    contact = state.get(STATE_CONTACT);
  }

  Uri getContact() {
    return contact;
  }

  void setContact(Uri contact) {
    this.contact = contact;
    state.set(STATE_CONTACT, contact);
  }
}
