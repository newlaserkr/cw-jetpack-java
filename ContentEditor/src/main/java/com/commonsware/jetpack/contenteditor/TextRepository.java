/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.contenteditor;

import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import androidx.lifecycle.LiveData;

class TextRepository {
  private static volatile TextRepository INSTANCE;

  synchronized static TextRepository get(Context context) {
    if (INSTANCE == null) {
      INSTANCE =
        new TextRepository((Application)context.getApplicationContext());
    }

    return INSTANCE;
  }

  private final Context context;
  private final ContentResolver resolver;
  private final Executor executor = Executors.newSingleThreadExecutor();

  private TextRepository(Application app) {
    this.context = app;
    resolver = context.getContentResolver();
  }

  LiveData<StreamResult> read(Uri source) {
    return new LiveStreamReader(source, resolver, executor);
  }

  LiveData<StreamResult> write(Uri source, String text) {
    return new LiveStreamWriter(source, resolver, executor, text, context);
  }

  private static class LiveStreamReader extends LiveData<StreamResult> {
    private final Uri source;
    private final ContentResolver resolver;
    private final Executor executor;

    LiveStreamReader(Uri source, ContentResolver resolver, Executor executor) {
      this.source = source;
      this.resolver = resolver;
      this.executor = executor;

      postValue(new StreamResult(true, null, null, null));
    }

    @Override
    protected void onActive() {
      super.onActive();

      executor.execute(() -> {
        try {
          postValue(new StreamResult(false, source,
            slurp(resolver.openInputStream(source)), null));
        }
        catch (FileNotFoundException e) {
          postValue(new StreamResult(false, source, "", null));
        }
        catch (Throwable t) {
          postValue(new StreamResult(false, null, null, t));
        }
      });
    }

    private String slurp(final InputStream is) throws IOException {
      final char[] buffer = new char[8192];
      final StringBuilder out = new StringBuilder();
      final Reader in = new InputStreamReader(is, StandardCharsets.UTF_8);
      int rsz = in.read(buffer, 0, buffer.length);

      while (rsz > 0) {
        out.append(buffer, 0, rsz);
        rsz = in.read(buffer, 0, buffer.length);
      }

      is.close();

      return out.toString();
    }
  }

  private static class LiveStreamWriter extends LiveData<StreamResult> {
    private final Uri source;
    private final ContentResolver resolver;
    private final Executor executor;
    private final String text;
    private Context context;

    LiveStreamWriter(Uri source, ContentResolver resolver, Executor executor,
                     String text, Context context) {
      this.source = source;
      this.resolver = resolver;
      this.executor = executor;
      this.text = text;
      this.context = context;

      postValue(new StreamResult(true, null, null, null));
    }

    @Override
    protected void onActive() {
      super.onActive();

      executor.execute(() -> {
        try {
          OutputStream os = resolver.openOutputStream(source);
          PrintWriter out = new PrintWriter(new OutputStreamWriter(os));

          out.print(text);
          out.flush();
          out.close();

          final String externalRoot =
            Environment.getExternalStorageDirectory().getAbsolutePath();

          if (source.getScheme().equals("file") &&
            source.getPath().startsWith(externalRoot)) {
            MediaScannerConnection
              .scanFile(context,
                new String[]{source.getPath()},
                new String[]{"text/plain"},
                null);
          }

          postValue(new StreamResult(false, source, text, null));
        }
        catch (Throwable t) {
          postValue(new StreamResult(false, null, null, t));
        }
      });
    }
  }
}
